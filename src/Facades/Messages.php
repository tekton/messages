<?php namespace Tekton\Messages\Facades;

class Messages extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'messages'; }
}
