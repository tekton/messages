Tekton Messages
===============

A simple component that adds a flash message handler, built with [Tekton\Session](https://gitlab.com/tekton/session).
